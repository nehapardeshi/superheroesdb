IF NOT EXISTS (SELECT * FROM sys.tables WHERE name = 'SuperheroPower')
BEGIN
	CREATE Table SuperheroPower
	(
		SuperHeroId int not null Foreign key references SuperHero (Id),
		PowerId int not null Foreign key references [Power] (Id)
	)

	ALTER TABLE SuperheroPower ADD PRIMARY KEY(SuperHeroId,PowerId)

END