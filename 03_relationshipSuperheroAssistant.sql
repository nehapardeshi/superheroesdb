IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = OBJECT_ID(N'Assistant') and name = 'SuperheroId')
BEGIN
	ALTER Table Assistant Add SuperheroId int
	ALTER TABLE Assistant ADD FOREIGN KEY (SuperheroId) REFERENCES Superhero(Id)
END