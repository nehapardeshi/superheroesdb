IF NOT EXISTS (SELECT * FROM sys.tables WHERE name = 'Superhero')
BEGIN
	CREATE Table Superhero
	(
		Id int primary key identity,
		Name varchar(100),
		Alias varchar(100),
		Origin varchar(100)
	)
END

IF NOT EXISTS (SELECT * FROM sys.tables WHERE name = 'Assistant')
BEGIN
	CREATE Table Assistant
	(
		Id int primary key identity,
		Name varchar(100)
	)
END

IF NOT EXISTS (SELECT * FROM sys.tables WHERE name = 'Power')
BEGIN
	CREATE Table [Power]
	(
		Id int primary key identity,
		Name varchar(100),
		Description varchar(100)
	)
END